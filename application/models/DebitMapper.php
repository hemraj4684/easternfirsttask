<?php

class Application_Model_DebitMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Credit');
        }
        return $this->_dbTable;
    }

    public function save(Application_Model_Credit $credit)
    {
        $amt =$credit->getAmount();
        $data = array(
            'name' => $credit->getName(),
            'amount' => $credit->getAmount(),
            'type'=> $credit->getType(),
            'date'=> $credit->getDate(),
            'userId'=>$credit->getUserId(),
            'currentAmount'=>$credit->getCurrentAmount() - $amt,
        );
        if($credit->getCurrentAmount() >= $amt) {
            $this->getDbTable()->insert($data);
        }
    }

}

