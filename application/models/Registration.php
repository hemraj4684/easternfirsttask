<?php

class Application_Model_Registration
{
    /**
    * @var string
    */
   protected $email;

   /**
    * @var string
    */
    protected $password;

   /**
    * @var string
    */
    protected $_firstName;

   /**
    * @var string
    */
    protected $_lastName;

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid registration property');
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid registration property');
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }


    public function setFirstName($text)
    {
        $this->_firstName = (string) $text;
        return $this;
    }

    public function getFirstName()
    {
        return $this->_firstName;
    }

    public function setLastName($text)
    {
        $this->_lastName = (string) $text;
        return $this;
    }

    public function getLastName()
    {
        return $this->_lastName;
    }
    /**
    * @return string
    */

    public function getEmail(): string
    {
        return $this->email;
    }

    /**
    * @param string $email
    */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
    * @return string
    */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
    * @param string $password
    */
    public function setPassword(string $password): void
    {
        $this->password = md5($password);
    }

}
