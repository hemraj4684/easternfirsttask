<?php

class Application_Model_ItemMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Item');
        }
        return $this->_dbTable;
    }

    public function save(Application_Model_Item $item)
    {
        $data = array(
            'name' => $item->getName(),
            'userId'=>$item->getUserId(),

        );
        return $this->getDbTable()->insert($data);
    }
}
