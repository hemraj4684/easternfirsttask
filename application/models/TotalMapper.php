<?php

class Application_Model_TotalMapper
{

    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Total');
        }
        return $this->_dbTable;
    }

    public function add(Application_Model_Credit $credit)
    {
        $storage = new Zend_Auth_Storage_Session();
        $data1 = $storage->read();

        $total = new Application_Model_DbTable_Total();

        $info = $total->fetchAll("userId = '$data1->id'");

        $Id = $info->userId;
        $amt = $credit->getAmount();
        $curramt=$credit->getCurrentAmount();
        $userCurrentAmount =  $curramt + $amt ;

        $uId = $data1->id;
        $userId = $credit->getUserId();

        if ($Id == $uId) {
            $data = [ 'amount' => $userCurrentAmount];
            return $this->getDbTable()->update($data, 'userId = ' . $uId);
        } else {
            $data2 =[
                'amount' =>  $amt,
                'userId'=> $userId,
                ];
            return $this->getDbTable()->insert($data2, 'userId = ' . $uId);
        }
    }

    public function sub(Application_Model_Credit $credit)
    {
        $storage = new Zend_Auth_Storage_Session();
        $data1 = $storage->read();

        $total = new Application_Model_DbTable_Total();

        $info = $total->fetchAll("userId = '$data1->id'");
        $Id = $info->userId;

        $amt = $credit->getAmount();
        $curramt=$credit->getCurrentAmount();
        $userCurrentAmount = $curramt-$amt;
        if ($userCurrentAmount < 0) {
            $userCurrentAmount = $curramt ;
        } else {
            $userCurrentAmount = $userCurrentAmount ;
        }
        $uId = $data1->id;
        $userId = $credit->getUserId();
        if ($Id == $uId) {
            $data = [ 'amount' => $userCurrentAmount ];
            return $this->getDbTable()->update($data, 'userId = ' . $uId);
        } else {
            $data2 = [
                'amount' => '0',
                'userId'=> $userId,
            ];
            return $this->getDbTable()->insert($data2, 'userId = ' . $uId);
        }
    }
}
