
<?php

class Application_Model_RegistrationMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Registration');
        }
        return $this->_dbTable;
    }

    public function save(Application_Model_Registration $registration)
    {
        $data = array(
            'firstName' => $registration->getFirstName(),
            'lastName' => $registration->getLastName(),
            'email'   => $registration->getEmail(),
            'password'=> $registration->getPassword(),

        );

        $this->getDbTable()->insert($data);
    }


}
