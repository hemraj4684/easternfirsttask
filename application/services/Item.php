<?php

class Application_Service_Item
{
    public function addItem($data)
    {
        $data = new Application_Model_Item($data);
        $mapper = new Application_Model_ItemMapper();
        $mapper->save($data);
    }
}
