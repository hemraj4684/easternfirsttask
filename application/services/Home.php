<?php

class Application_Service_Home
{
  /**
   * @return string
   */
    public function showName(): string
    {
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if (!$data){
            $this->redirect('auth');
        }
        $userName = $data->firstName;
        return $userName;
    }

    /**
     * @return array
     */

    public function creditTransction()
    {
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        $creditTransction = new Application_Model_DbTable_Credit();
        $select = $creditTransction->select()->where("type = '1'")
                                             ->where("userId = '$data->id'")
                                             ->order('id DESC');
        $transaction =  $creditTransction->fetchAll($select);
       return $transaction;
    }

    public function debitTransction()
    {
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        $creditTransction = new Application_Model_DbTable_Credit();
        $debit = $creditTransction->select()->where("type = '0'")
                                            ->where("userId = '$data->id'")
                                            ->order('id DESC');
        $transction =  $creditTransction->fetchAll($debit);
        return $transction;
    }

    public function showTotal()
    {
        $UAmount =  '0';

        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();

        $amount = new Application_Model_DbTable_Total();
        $userAmount = $amount->fetchRow("userId = '$data->id'");

        $Id =$userAmount->userId;
        $uId = $data->id;
        if ($Id == $uId) {
            $showAmount = $userAmount->amount;
        } else {
            $showAmount = $UAmount;
        }
        return $showAmount;
    }
}
