<?php

class Application_Service_Auth
{
    public function login($data){
        $users = new Application_Model_DbTable_Login();

        $auth = Zend_Auth::getInstance();
        $authAdapter = new Zend_Auth_Adapter_DbTable($users->getAdapter(),'users');

        $authAdapter->setIdentityColumn('email')
            ->setCredentialColumn('password');

        $authAdapter->setIdentity($data['email'])
            ->setCredential(md5($data['password']));
        $result = $auth->authenticate($authAdapter);
        
        if ($result->isValid()) {
            $storage = new Zend_Auth_Storage_Session();
            $storage->write($authAdapter->getResultRowObject());
        }
    }
}
