<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initDoctype()
    {
       $this->bootstrap('layout');
       $layout = $this->getResource('layout');
    }

    protected function _initResourceLoader()
    {
        $this->_resourceLoader->addResourceType( 'service', 'services', 'Service' );
    }
}
