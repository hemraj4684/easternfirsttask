<?php

class Application_Form_Registration extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');
        $this->addElement(
            'text',
            'firstName',
            [
            'placeholder' => 'First Name',
            'label' => 'First Name *',
            'required' => true,
            'validators' => [
                ['validator' => 'StringLength', 'options' => [2, 20]],
                ['Regex', false,['pattern' => '/^[a-zA-Z\s]+$/',
                'messages' => 'Only characters are allowed ']]
            ]]
        );

        $this->addElement(
            'text',
            'lastName',
            [
            'placeholder' => 'Last Name',
            'label' => 'Last Name *',
            'required' => true,
            'validators' => [
                ['validator' => 'StringLength', 'options' => [2, 20]],
                ['Regex', false,['pattern' => '/^[a-zA-Z\s]+$/',
                'messages' => 'Only characters are allowed ']]
            ]]
        );

        $this->addElement(
            'text',
            'email',
            [
            'placeholder' => 'Enter Email',
            'label' => 'Email *',
            'required' => true,
            'validators' => [
            'EmailAddress',
            ]]
        );

        $this->addElement(
            'password',
            'password',
            [
            'placeholder' => 'Enter Password',
            'messages' => 'password should contain atleast 6 char long ,1 uppercase ,
                1 lowercase and 1 digit or special charcter',
            'label' => 'Password *',
            'required' => true,
            'validators' =>[
                ['validator' => 'StringLength', 'options' => [0, 20]],
                ['Regex', false, ['pattern' =>
                '/(?-i)(?=^.{6,}$)((?!.*\s)(?=.*[A-Z])(?=.*[a-z]))((?=(.*\d){1,})|(?=(.*\W){1,}))^.*$/',
                'messages' => 'password should contain atleast 6 char long ,1 uppercase ,
                1 lowercase and 1 digit or special charcter',
                ]]
            ]]
        );

        $this->addElement(
            'password',
            'verifypassword',
            [
            'label'      => 'Verify Password *',
            'placeholder' => 'Re-enter Password',
            'required'   => true,
            'validators' => [
                ['identical', true,['password',
                 'messages' => 'Password and Confirm password does not match']]
                ]]
        );

        $this->addElement(
            'submit',
            'submit',
            [
                'ignore' => true,
                'label' => 'Registration',
            ]
        );
    }
}
