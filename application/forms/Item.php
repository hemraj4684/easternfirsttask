<?php

class Application_Form_Item extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Name')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');
        $userId = new Zend_Form_Element_Hidden('userId');
        $userId->setValue($data->id);
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Add Item');
        $this->addElements([$name,$submit,$userId]);
    }
}
