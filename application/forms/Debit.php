<?php

class Application_Form_Debit extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');

        $currentDate = date('Y/m/d h:i:s ', time());

        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();

        $items1 = new Application_Model_DbTable_Item();
        $names = $items1->fetchAll("userId = '$data->id'");

        foreach ($names as $row) {
            $return [$row['name']] = $row['name'];
        }
        //echo '<pre>';var_dump($return);

        $amount = new Application_Model_DbTable_Total();
        $userAmount = $amount->fetchRow("userId = '$data->id'");
        $currAmt = $userAmount->amount;

        $items = ["multiOptions"=>
            $return
        ];
        $itemElement = new Zend_Form_Element_Select('name', $items);
        $itemElement->setLabel("Item:")
            ->setRequired(true);
        $amount = new Zend_Form_Element_Text('amount');
        $amount->setLabel('Amount')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Debit');

        //create hidden input
        $type = new Zend_Form_Element_Hidden('type');
        $type->setValue('0');

        $date = new Zend_Form_Element_Hidden('date');
        $date->setValue($currentDate);

        $userId = new Zend_Form_Element_Hidden('userId');
        $userId->setValue($data->id);

        $currentAmount = new Zend_Form_Element_Hidden('currentAmount');
        $currentAmount->setValue($currAmt);

        $this->addElements(array($itemElement, $amount, $submit,$date,$type, $userId, $currentAmount));
    }
}
