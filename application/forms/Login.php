<?php

class Application_Form_Login extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');

        $this->addElement(
            'text',
            'email',
            [
                'label' => 'Email *',
                'required' => true,
                'validators' => ['EmailAddress']
            ]
        );

        $this->addElement(
            'password',
            'password',
            [
              'label' => 'Password *',
              'required' => true,
              'validators' => [
                  ['validator' => 'StringLength', 'options' => [0, 20]]
              ]
            ]
        );

        $this->addElement(
            'submit',
            'submit',
            [
              'ignore'   => true,
              'label'    => 'Login',
            ]
        );
    }
}
