<?php

class CreditController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $form = new  Application_Form_Credit();
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            if ($form->isValid($data)) {
                $service = new Application_Service_Credit();
                $service->transction($data);
                $this->redirect('home');
            }
        }
        $this->view->form = $form;
    }
}
