<?php

class RegisterController extends Zend_Controller_Action
{

/**
 * @return [type] [description]
 */
    public function indexAction()
    {
        $request = $this->getRequest();
        $form    = new Application_Form_Registration();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($form->isValid($data)) {
                $service = new Application_Service_Register();
                $service->register($data);
                $this->redirect('auth');
            }
        }
        
        $this->view->form = $form;
    }
}
