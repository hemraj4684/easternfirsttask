<?php

class ItemController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $form = new  Application_Form_Item();
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            if ($form->isValid($postData)) {
                $service = new Application_Service_Item();
                $service->addItem($postData);
                $this->redirect('credit');
            }
        }
        $this->view->form = $form;
    }
}
