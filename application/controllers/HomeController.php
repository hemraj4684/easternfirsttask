<?php

class HomeController extends Zend_Controller_Action
{
    public function init()
    {

    }

    public function indexAction()
    {
        $service = new Application_Service_Home();

        $userName= $service->showName();
        $this->view->firstName = $userName;

        $creditTransction = $service->creditTransction();
        $this->view->creditTransction = $creditTransction;

        $debitTransction = $service->debitTransction();
        $this->view->debitTransction = $debitTransction;

        $total = $service->showTotal();
        $this->view->amount = $total;
    }
}
