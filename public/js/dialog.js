$(document).ready(function() {
    $('#wrapper').dialog({
        autoOpen: false,
        title: 'Credit',
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        },
        width: 500,
        height: 350,
    });
    $('#opener').click(function() {
        $('#wrapper').dialog('open');
    });
    $('#wrapper').validate({
        rules: {
            name: { required: true },
            amount: {
                required:true,
                money: true
            }
        },
    });
});
